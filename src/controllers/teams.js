const { conn } = require('../database/connection');

module.exports = class TeamController {

    async getAll() {
        try {
            const sqlQuery = 'SELECT * FROM teams;';
            const result = await conn.query(sqlQuery);
            if (result.length <= 0) {
                return [];
            } else {
                return result;
            }
        } catch (error) {
            throw error;
        }
    }

}