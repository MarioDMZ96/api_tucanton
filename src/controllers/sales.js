const { conn } = require('../database/connection');

module.exports = class SalesController {

    async getSalesByTeam(id) {
        try {
            const sqlQuery = `SELECT 
                team.name as nombre, 
                COUNT(sale.id) as ventas_totales
                FROM sales sale
                INNER JOIN teams team
                ON team.id = (SELECT teamId from users WHERE id = sale.userId)
                WHERE team.id = ${id}`;
            const result = await conn.query(sqlQuery);
            if (!result) {
                return [];
            } else {
                return result;
            }
        } catch (error) {
            throw error;
        }
    }

    async getSalesByUser(id) {
        try {
            const sqlQuery = `SELECT 
                usr.name as nombre, 
                COUNT(sale.id) as ventas_totales
                FROM sales sale
                INNER JOIN users usr
                ON usr.id = sale.userId
                WHERE usr.id = ${id}`;
            const result = await conn.query(sqlQuery);
            if (!result) {
                return [];
            } else {
                return result;
            }
        } catch (error) {
            throw error;
        }
    }
}