const { conn } = require('../database/connection');

module.exports = class ClientsController {

    async getAll (){
        try {
            const sqlQuery = 'SELECT * FROM clients;';
            const result = await conn.query(sqlQuery);
            if ( result.length > 0 ){
                return result;
            } else {
                return [];
            }
        } catch (error) {
            throw error;
        }
    }

    async getOneById (id){
        try {
            const sqlQuery = `SELECT * FROM clients WHERE id = ${id};`;
            const result = await conn.query(sqlQuery);
            if ( !result[0] ){
                return [];
            } else {
                return result[0];
            }
        } catch (error) {
            throw error;
        }
    }
}
