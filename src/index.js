// import packages
const express = require('express');
const cors = require('cors');
require('dotenv').config();

const app = express();

// app middlewares
app.use( cors() );
app.use( express.json() );
app.use( express.urlencoded({ extended: false }) );

// app routes
const routes = require('./routes/index');
app.use( '/', routes );

// server start
const port = process.env.APP_PORT;
app.listen( port, () => console.log(`Server listen on: http://localhost:${port}`) );