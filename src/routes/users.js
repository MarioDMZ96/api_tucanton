const express = require('express');
const router = express.Router();

const UserController = require('../controllers/users');
const userController = new UserController();

router.get( '/users', async (req, res) => {
    let result = await userController.getAll();
    res.json({
        status: 'ok', 
        message: 'users list', 
        users: result
    });
});

router.get( '/users/:id', async (req, res) => {
    const clientId = req.params.id;
    let result = await userController.getOneById(clientId);
    res.status(200).json({
        status: 'ok',
        message: 'user info', 
        user: result
    });
});

module.exports = router;