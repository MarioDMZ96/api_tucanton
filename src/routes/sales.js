const express = require('express');
const router = express.Router();

const SalesController = require('../controllers/sales');
const salesController = new SalesController();

router.get( '/sales/team/:id', async (req, res) => {
    const teamId = req.params.id;
    let result = await salesController.getSalesByTeam(teamId);
    res.json({
        status: 'ok', 
        message: 'sales list', 
        sales: result
    });
});

router.get( '/sales/users/:id', async (req, res) => {
    const userId = req.params.id;
    let result = await salesController.getSalesByUser(userId);
    res.status(200).json({
        status: 'ok',
        message: 'sales info', 
        sales: result
    });
});

module.exports = router;