const express = require('express');

const app = express();

// routes
const clientsRouter = require('./clients');
const usersRouter = require('./users');
const salesRouter = require('./sales');
const teamsRouter = require('./teams');

app.use( '/api', clientsRouter );
app.use( '/api', usersRouter );
app.use( '/api', salesRouter );
app.use( '/api', teamsRouter );

module.exports = app;