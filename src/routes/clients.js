const express = require('express');
const router = express.Router();

const ClientsController = require('../controllers/clients');
const clientController = new ClientsController();

router.get( '/clients', async (req, res) => {
    let result = await clientController.getAll();
    res.status(200).json({
        status: 'ok',
        message: 'clients list', 
        clients: result
    });
});

router.get( '/clients/:id', async (req, res) => {
    const clientId = req.params.id;
    let result = await clientController.getOneById(clientId);
    res.status(200).json({
        status: 'ok',
        message: 'client info', 
        client: result
    });
});

module.exports = router;