const express = require('express');
const router = express.Router();

const TeamController = require('../controllers/teams');
const teamController = new TeamController();

router.get( '/teams', async (req, res) => {
    let result = await teamController.getAll();
    res.json({
        status: 'ok', 
        message: 'teams list', 
        teams: result
    });
});


module.exports = router;