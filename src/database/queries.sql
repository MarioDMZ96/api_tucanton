/*CONSULTA DE DETALLE DE VENTAS POR USUARIO*/
SELECT 
    sale.id as id_venta,
    sale.createdAt as fecha_venta,
    cl.name as nombre_cliente,
    sale.amount as monto,
    team.name as nombre_equipo,
    usr.name as nombre_usuario
FROM sales sale
INNER JOIN clients cl
    ON sale.clientId = cl.id
INNER JOIN users usr
    ON sale.userId = usr.id
INNER JOIN teams team
    ON usr.teamId = team.id;

/*VENTAS POR EQUIPO*/
SELECT 
    team.name as nombre_equipo, 
    COUNT(sale.id) as ventas_totales
FROM sales sale
INNER JOIN teams team
ON team.id = (SELECT teamId from users WHERE id = sale.userId)
GROUP BY team.name;


/*VENTAS POR USUARIO*/
SELECT 
    usr.name as nombre_usuario, 
    COUNT(sale.id) as ventas_totales
FROM sales sale
INNER JOIN users usr
ON usr.id = sale.userId
GROUP BY usr.name;

/*VENTAS POR MES*/
SELECT 
    month(sale.createdAt) as mes, 
    COUNT(sale.id) as ventas_totales
FROM sales sale
GROUP BY month(sale.createdAt);