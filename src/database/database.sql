CREATE TABLE teams (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(25) NOT NULL
);

CREATE TABLE users (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(25) NOT NULL,
  teamId INT NOT NULL,
  FOREIGN KEY (teamId)
        REFERENCES teams (id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

CREATE TABLE clients (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(25) NOT NULL
);

CREATE TABLE sales (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  createdAt DATETIME NOT NULL,
  amount FLOAT NOT NULL,
  clientId INT NOT NULL,
  userId INT NOT NULL,
  FOREIGN KEY (clientId)
        REFERENCES clients (id)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
  FOREIGN KEY (userId)
        REFERENCES users (id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

INSERT INTO teams (id,name) VALUES ( NULL, 'Team_1' ),( NULL, 'Team_2' );

INSERT INTO users (id, name, teamId) VALUES ( NULL, 'User 1', 1 ), ( NULL, 'User 2', 1 ), ( NULL, 'User 3', 2 ),( NULL, 'User 4', 2 );

INSERT INTO clients (id, name) VALUES ( NULL, 'Client_1' ),( NULL, 'Client_2' ),( NULL, 'Client_3' ),( NULL, 'Client_4' );

INSERT INTO sales (id, createdAt, amount, clientId, userId) VALUES 
( NULL, '2021-05-03 12:30:00', 35000, 1, 4 ),
( NULL, '2021-05-04 12:30:00', 20000, 1, 4 ),
( NULL, '2021-05-05 12:30:00', 25000, 2, 3 ),
( NULL, '2021-05-06 12:30:00', 11000, 2, 3 ),
( NULL, '2021-07-06 12:30:00', 10000, 2, 3 ),
( NULL, '2021-06-05 12:30:00', 40000, 3, 2 ),
( NULL, '2021-07-06 12:30:00', 10000, 3, 2 ),
( NULL, '2021-07-06 12:30:00', 10000, 4, 1 );