const mysql = require('mysql');
const config = require('./config');

const { promisify } = require('util');
const conn = mysql.createPool(config);

conn.getConnection( (err,connection) => {
  if(err){
    if(err.code === 'PROTOCOL_CONNECTION_LOST'){
      console.error('Connection closed');
    }
    if(err.code === 'ER_CON_COUNT_ERROR'){
      console.error('Database does not attend the request');
    }
    if(err.code === 'ECONNREFUSED'){
      console.error('Connection refused');
    }
    console.log(err);
  }
  if(connection){
    connection.release();
    console.log('database connect');
    return;
  }
});

conn.query = promisify(conn.query);

module.exports = {
  conn
}