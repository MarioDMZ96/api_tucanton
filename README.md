# Ventas API

In the project directory, to install the project dependencies you can run:
```
    npm install
```

In the project directory, to run the project you can run:

```
    npm start
```

## In your DBMS you can create a database, and run the database.sql file that is in database directory to create tables and populating.

## In the request.http file, you can test the api with the vs code extension Rest client
